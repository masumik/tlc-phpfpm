FROM php:7-fpm-alpine

COPY conf/php.ini /usr/local/etc/php

RUN set -x && \
  apk add --no-cache vim git icu-libs fontconfig libintl libpng libjpeg-turbo imagemagick freetype && \
  apk add --no-cache --virtual build-dependencies icu-dev autoconf gcc g++ make libtool imagemagick-dev libpng-dev libjpeg-turbo-dev freetype-dev && \
  pecl install imagick && \
  pecl install grpc && \
  pecl install -o -f redis && \
  pecl install xdebug && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ && \
  docker-php-ext-install -j${NPROC} intl && \
  docker-php-ext-install -j${NPROC} pdo_mysql && \
  docker-php-ext-install -j${NPROC} gd && \
  docker-php-ext-install -j${NPROC} zip opcache && \
  docker-php-ext-enable imagick && \
  docker-php-ext-enable grpc && \
  docker-php-ext-enable redis && \
  docker-php-ext-enable xdebug \
  && sed -i '1 a xdebug.remote_autostart=true' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_mode=req' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_handler=dbgp' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_connect_back=0 ' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.idekey="PHPSTORM" ' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_host=10.255.255.128' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_port=9001' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && sed -i '1 a xdebug.remote_enable=1' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \  
  rm -rf /tmp/pear && \
  apk del --no-cache --purge build-dependencies && \
  curl -s https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer 
